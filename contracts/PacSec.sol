// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract PacSec is Ownable, AccessControl {

  bytes32 internal constant TEAM_MEMBER = keccak256("TEAM_MEMBER");
  bytes32 internal constant BLACKLISTED = keccak256("BLACKLISTED");

  event TeamMemberAdded(address indexed _by, address indexed _address);
  event TeamMemberRemoved(address indexed _by, address indexed _address);
  event BlacklistedWalletAdded(address indexed _by, address indexed _address);
  event BlacklistedWalletRemoved(address indexed _by, address indexed _address);

  constructor()
  {
    // Setup Default admin for the project
    _setupRole(DEFAULT_ADMIN_ROLE, msg.sender);

    // Add Self in as the first team member
    _setupRole(TEAM_MEMBER, msg.sender);
  }

  function isTeamMember(
    address _address
  ) public view returns (bool)
  {
    return hasRole(TEAM_MEMBER, _address);
  }

  function isBlacklisted(
    address _address
  ) public view returns (bool)
  {
    return hasRole(BLACKLISTED, _address);
  }

  function isRole(
    bytes32 role,
    address _address
  ) public view returns (bool) {
    return hasRole(role, _address);
  }

  // Team Member Methods

  function addTeamMember(
    address _address
  ) public onlyRole(TEAM_MEMBER) {
    grantRole(TEAM_MEMBER, _address);
    emit TeamMemberAdded(msg.sender, _address);
  }

  function addTeamMembers(
    address[] memory _addresses
  ) public onlyRole(TEAM_MEMBER) {
    for (uint256 i; i < _addresses.length; i++) {
      if(isTeamMember(_addresses[i]) == false) {
        addTeamMember(_addresses[i]);
      }
    }
  }

  function removeTeamMember(
    address _address
  ) public onlyRole(TEAM_MEMBER) {
    _revokeRole(TEAM_MEMBER, _address);
    emit TeamMemberRemoved(msg.sender, _address);
  }

  // Blacklist Methods

  function blacklistWalletAddress(
    address _address
  ) public onlyRole(TEAM_MEMBER) {
    grantRole(BLACKLISTED, _address);
    emit BlacklistedWalletAdded(msg.sender, _address);
  }

  function blacklistAddresses(
    address[] memory _addresses
  ) public onlyRole(TEAM_MEMBER) {
    for (uint256 i; i < _addresses.length; i++) {
      if(isBlacklisted(_addresses[i]) == false) {
        blacklistWalletAddress(_addresses[i]);
      }
    }
  }

  function removeBlacklistedWallet(
    address _address
  ) public onlyRole(TEAM_MEMBER) {
    _revokeRole(BLACKLISTED, _address);
    emit BlacklistedWalletRemoved(msg.sender, _address);
  }

  // Basic withdraw function to withdraw matic, it shouldn't be needed but added just in case
  function withdraw() public payable onlyRole(TEAM_MEMBER)
  {
    (bool os, ) = payable(owner()).call{value: address(this).balance}("");
    require(os);
  }
}

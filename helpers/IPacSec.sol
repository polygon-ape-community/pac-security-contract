// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.4.22 <0.9.0;

interface IPacSec {
  function isTeamMember(address _account) external view returns (bool);
  function isBlacklisted(address _account) external view returns (bool);
  function isRole(bytes32 role, address _address) external view returns (bool);
}

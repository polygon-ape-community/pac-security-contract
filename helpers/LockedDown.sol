// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.4.22 <0.9.0;

import "./IPacSec.sol";


abstract contract LockedDown {

    address private PacSecAddress;

    constructor() {
    }

    modifier onlyTeam()
    {
        if (!IPacSec(PacSecAddress).isTeamMember(msg.sender)) {
            revert(
            string(
                abi.encodePacked("Account not a member of the team!!")
            )
            );
        }
        _;
    }

    function setPacSecAddress(
        address _PacSecAddress
    ) public virtual
    {
        require(PacSecAddress != _PacSecAddress, "Address can't be the same");

        _setPacSecAddress(_PacSecAddress);
    }

    function _setPacSecAddress(
        address _PacSecAddress
    ) internal virtual
    {
        PacSecAddress = _PacSecAddress;
    }

    function isRole(
        bytes32 role,
        address _address
    ) public virtual returns (bool) {
        return IPacSec(PacSecAddress).isRole(role, _address);
    }

    function isBlacklisted(
        address _address
    ) public virtual returns (bool)
    {
        return IPacSec(PacSecAddress).isBlacklisted(_address);
    }

    function isTeamMember(
        address _address
    ) public virtual returns (bool)
    {
        return IPacSec(PacSecAddress).isTeamMember(_address);
    }
}

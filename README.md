# PacSec (A PAC Security contract )

This contract has been designed to be used as an Interface for all further contracts for PAC.

When making a new contract please ensure you copy both the "IPacSec.sol" & "LockedDown.sol" into the new contract. (An example of this can be foinf in the Pac & Friends contract).

The nature of this contract is to lock down any new contracts moving forward to only allow team members to interact with them, by creating a contract to handle this, we can simply call the predeplyed contract to make checks and should a team member leave or thier wallet become comprimised the wallet address will only need to be changed here and it will update all other contracts to disable or enable that wallet.

# Deploying The Contract

I have already deployed this contract to the testnet using truffle. To deploy using truffle simple install the latest version of "NodeJS", open the project in your favourite Terminal program and run the below command.

```bash
npm install
```
Once the dependancies have installed you will have a "node_modules" folder added into the root of the project.

After the above has finished run the following command to open up the truffle dashboard
```bash
npm run dashboard
```
This will give you a URL that will allow you to sign transactions during the deployment using MetaMask, this is far more secure than the previously mentioned step thus the change.

To deploy the project, please make sure you're connected to the correct network in MetaMask and you have enough to cover the Gas, then run the following command
```bash
npm run deploy
```
You should get a signature request in your truffle dashboard window, simply accept this and allow the transaction to process
---

# Interacting with the contract

To interact with the contract, you can do one of 2 things;

1. Verify the contract on the relevant network and use the graphical interface provided (https://mumbai.polygonscan.com/).
2. Use the command line interface provided by truffle.

## Command Line Interface

When the contract has been deployed to the relevant network, run the command below to initiate a console for interacting with the contract;

```bash
npm run console
```
from this you will be able to create an instance of the contract to interact with, lets do that now. 
Run the below command in the console and we will assign the contract to a variable called "instance".

```bash
const instance = await PacSec.deployed();
```

Now we have the "instance" variable setup to can use an of the functions on the contract like so

```bash
await instance.isTeamMember("YourWalletAddressHere");
```
Any public facing functions that do not add more data to the blockchain do not require any gas and thus will return the expected value, in this case above it should return "true"

Using functions that store data to the blockchain will require gas, and will need to be signed for using the dashboard screen, command like that are ran like the below.

```bash 
instance.addTeamMember("0x59F6d9796A2Df5EC202cbC10Ed0aAA100685b32e");
```

# Public Useable Functions

The public functions are the ones anyone can access, these DO NOT write to the contract and thus don't charge any gas. they are simply LOOKUP transactions.

## isTeamMember

The is team member function will allow the general public to enter in a wallet address and check wether it is actually an address assigned to the team.
This is also used as a modifier on the contracts moving forward to allow only team members to access certain functions.

**Return Type:** Bool (true/false)

## isBlacklisted
A way for the general public to check to see if a wallet address has been blacklisted from any futu

**Parameters required:**

* Wallet Address

**Return Type:** Bool (true/false)

## isRole

Not really needed however may serve a purpose, this will allow a user to check a certain roles byte string vs a wallet address to see if it's a member of that role.

**Parameters required:**

* Role (needs to be the byte string of the role)
* Address (Wallet address to lookup)

**Return Type:** Bool (true/false)

---

# Team Member Functions

All team member functions here are only useable by users that are members of the team itself, the wallet addresses would need to be added to the "Team Member" role in order to interact with them.

The default and first team member is that one that has deployed the contract, and will be responsible for adding more team members.

## addTeamMember
The only users that can add in new team members are the current team members list, by default the first team member will be the contract deployer.

To add a new team member simply add in the wallet address of the team member to this contract in PolyScan, click "Write" and pay the gas fees associated with the transaction.

**Parameters required:**
* Address (Wallet address to add as a team member)

**Return Type:** none

## addTeamMembers
As with the above, the only users that can add in new team members are the current team members list, by default the first team member will be the contract deployer.

To add a new team members simply add in the wallet addresses in an array format, (like we did with the backlisting of the wives contract) of the team members to this contract in PolyScan, click "Write" and pay the gas fees associated with the transaction.

**Parameters required:**
* Addresses (array of wallet addresses 1 for each member)

**Return Type:** none

## removeTeamMember

If in the event a team member is removed or decides to leave, use this function to prevent them from performing and team role functions on any of the contracts that have been deployed using this interface.

**Parameters required:**
* Address (Wallet address of the team member being removed)

**Return Type:** none

## blacklistWalletAddress

To blacklist a wallet address from future transactions simply add in the wallet address into the field provided on this contract in PolyScan, click "Write" and pay the gas fees associated with the transaction.

**Parameters required:**
* Address (Wallet address to add to the blacklist)

**Return Type:** none

## blacklistAddresses

To blacklist multiple addresses simply add in the wallet addresses in an array format, (like we did with the backlisting of the wives contract) in the field provided on this contract in PolyScan, click "Write" and pay the gas fees associated with the transaction.

**Parameters required:**
* Addresses (array of wallet addresses)

**Return Type:** none

## removeBlacklistedWallet

Just in case a wallet address has been added by accident, or you simple want to remove a wallet address form the blacklist, you can.

**Parameters required:**
* Address (Wallet address to be removed to the blacklist)

**Return Type:** none
